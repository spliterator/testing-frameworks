package com.butilov;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.butilov")
public class SpringConfiguration {

}
package com.butilov.service;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Qualifier("producer2")
public class ValueProducerImpl2 implements ValueProducer {

    @Override
    public int getValue() {
        return 2;
    }
}

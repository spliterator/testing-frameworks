package com.butilov.service;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Qualifier("producer1")
public class ValueProducerImpl1 implements ValueProducer {

    @Override
    public int getValue() {
        return 1;
    }
}

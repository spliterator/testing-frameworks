package com.butilov.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
@RequiredArgsConstructor
public class ValueValidator {

    @Qualifier("producer2")
    private final ValueProducer valueProducer;

    @PostConstruct
    void print() {
        int value = valueProducer.getValue();
        System.out.println("get value " + value);
    }
}
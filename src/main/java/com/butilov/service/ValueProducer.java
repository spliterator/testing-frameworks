package com.butilov.service;

public interface ValueProducer {

    int getValue();
}